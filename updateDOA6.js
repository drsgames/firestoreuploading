//const dataForDOA6 = require("C:\Users\yetim\StudioProjects\fighting-game-buddy\app\src\main\assets\deadOrAlive6\*");
const doa6 = ('./doa6Characters.json')

const admin = require('./node_modules/firebase-admin');
const serviceAccount = require("./serviceAccountKey.json");
const collectionKey = "attack_list"; //name of the collection

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://fighting-game-buddy.firebaseio.com"
});
const firestore = admin.firestore();
const settings = { timestampsInSnapshots: true };
firestore.settings(settings);

const filesLocation = "C:\Users\yetim\StudioProjects\fighting-game-buddy\app\src\main\assets\deadOrAlive6\"

for (character in doa6.characters) {
    /*
     * parse character data into an array of str({"name" + ".json"})
     * open each file in array and parse that data into 
     * : "game", "version", "character", "timestamp", data
    */
    $.getJSON(filesLocation + character + ".json", function (json) {
        console.log(json);
        for (s in json.command_list) {
            firestore.collection(collectionKey).add({
                game: "Dead or Alive 6",
                character: json.name.toLowerCase(),
                version: json.version,
                timestamp: json.edited,
                attack_command: JSON.stringify(command_list[s].attack_command),

                damage: JSON.stringify(command_list[s].damage),
                type: JSON.stringify(command_list[s].type),
                level: JSON.stringify(command_list[s].level),
                frame: JSON.stringify(command_list[s].frame),
                frame_startup: JSON.stringify(command_list[s].frame_startup),
                frame_hit: JSON.stringify(command_list[s].frame_hit),
                frame_recovery: JSON.stringify(command_list[s].frame_recovery),
                frame_end: JSON.stringify(command_list[s].frame_end),
                advantage_hit: JSON.stringify(command_list[s].advantage_hit),
                advantage_counter: JSON.stringify(command_list[s].advantage_counter),
                advantage_hi_counter: JSON.stringify(command_list[s].advantage_hi_counter),
                advantage_guard: JSON.stringify(command_list[s].advantage_guard),
                gauge_hit: JSON.stringify(command_list[s].gauge_hit),
                gauge_counter: JSON.stringify(command_list[s].gauge_counter),
                gauge_hi_counter: JSON.stringify(command_list[s].gauge_hi_counter),
                gauge_guard: JSON.stringify(command_list[s].gauge_guard),
                tag: JSON.stringify(command_list[s].tag),
                notes: JSON.stringify(command_list[s].notes)
            })
                .then(function (docRef) {
                    console.log("Document written with ID: ", docRef.id);
                })
                .catch(function (error) {
                    console.error("Error adding document: ", error);
                });
        }
            
    });
}

    //var docRef = firestore.collection(collectionKey).where("game", "==", data.game).where("character", "==", data.character).where("attack_command", "==", data.attack_command);
    //docRef.get().then(function (doc) {
    //    if (doc.exists) {
    //        if ((parseInt(doc.data().edited) - parseInt(data.edited)) < 0) {
    //            docRef.set(data)
    //        } else {
    //            console.log("Document up-to-date");
    //        }
    //    } else {
    //        console.log("No such document! " + JSON.stringify(data.attack_command) + "created!");
    //        firestore.collection(collectionKey).doc().set(data)
    //            .then((res) => {
    //                console.log("Document successfully written!");
    //            })
    //            .catch((error) => {
    //                console.error("Error writing document: ", error);
    //            });
    //    }
    //}).catch(function (error) {
    //    console.log("Error getting document:", error);
    //});
}