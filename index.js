const admin = require('./node_modules/firebase-admin');
const serviceAccount = require("./serviceAccountKey.json");
const heihachi = require("./heihachi.json");
const video180 = require("./HEIHACHI_VIDEO180.json");
const video360 = require("./HEIHACHI_VIDEO360.json");
const video720 = require("./HEIHACHI_VIDEO720.json");
const collectionKey = "attack_list"; //name of the collection
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://fighting-game-buddy.firebaseio.com"
});
const firestore = admin.firestore();
const settings = { timestampsInSnapshots: true };
firestore.settings(settings);

const attack_list = heihachi.attack_list;
for (s in heihachi.attack_list) {
    //var data = {
    //    game: "Tekken 7",
    //    character: heihachi.name,
    //    version: heihachi.version,
    //    edited: heihachi.edited,
    //    attack_command: attack_list[s].attack_command,
    //    attack_name: attack_list[s].attack_name,
    //    jp_command: attack_list[s].jp_command,
    //    jp_name: attack_list[s].jp_name,
    //    property: attack_list[s].property,
    //    damage: attack_list[s].damage,
    //    startup: attack_list[s].startup,
    //    block: attack_list[s].block,
    //    hit: attack_list[s].hit,
    //    counter_hit: attack_list[s].counter_hit,
    //    tag: attack_list[s].tag,
    //    notes: attack_list[s].notes,
    //    index: attack_list[s].index,
    //    video180: video180[attack_list[s].attack_command],
    //    video360: video360[attack_list[s].attack_command],
    //    video720: video720[attack_list[s].attack_command]
    //};
    //console.log(data);
    firestore.collection(collectionKey).add({
        game: "Tekken 7",
        character: heihachi.name,
        version: heihachi.version,
        edited: heihachi.edited,
        attack_command: JSON.stringify(attack_list[s].attack_command),
        attack_name: attack_list[s].attack_name,
        jp_command: JSON.stringify(attack_list[s].jp_command),
        jp_name: attack_list[s].jp_name,
        property: attack_list[s].property,
        damage: attack_list[s].damage,
        startup: attack_list[s].startup,
        block: attack_list[s].block,
        hit: attack_list[s].hit,
        counter_hit: attack_list[s].counter_hit,
        tag: attack_list[s].tag,
        notes: JSON.stringify(attack_list[s].notes),
        index: attack_list[s].index,
        video180: video180[attack_list[s].attack_command],
        video360: video360[attack_list[s].attack_command],
        video720: video720[attack_list[s].attack_command]
    })
        .then(function (docRef) {
            console.log("Document written with ID: ", docRef.id);
        })
        .catch(function (error) {
            console.error("Error adding document: ", error);
        });


    //var docRef = firestore.collection(collectionKey).where("game", "==", data.game).where("character", "==", data.character).where("attack_command", "==", data.attack_command);
    //docRef.get().then(function (doc) {
    //    if (doc.exists) {
    //        if ((parseInt(doc.data().edited) - parseInt(data.edited)) < 0) {
    //            docRef.set(data)
    //        } else {
    //            console.log("Document up-to-date");
    //        }
    //    } else {
    //        console.log("No such document! " + JSON.stringify(data.attack_command) + "created!");
    //        firestore.collection(collectionKey).doc().set(data)
    //            .then((res) => {
    //                console.log("Document successfully written!");
    //            })
    //            .catch((error) => {
    //                console.error("Error writing document: ", error);
    //            });
    //    }
    //}).catch(function (error) {
    //    console.log("Error getting document:", error);
    //});
}